pintarTable();

function pintarTable(){
    if( localStorage.getItem('mayorEdad')=='0' ){
        pintarTableCocteles();
        $("#accordionCervezas").css("display", "none");
        $("#accordionTradicionalCerveza").css("display", "none");
    }else{
        pintarTableCervezas();
        pintarTableCocteles();
        pintarTableTradicionalCerveza();
    }
}

async function pintarTableCervezas(){
    let data = await peticionCervezas();
    const tbody = document.getElementById('bodyCervezas');
    tbody.innerHTML = '';
    data.forEach(atencion => {
        let precio = precioRandom();
        const tr = document.createElement('tr');
        tr.ondblclick = () => {
            aggTableCompra(atencion.name,atencion.city,precio);
        }
        tr.innerHTML = `
            <td>${atencion.name}</td>
            <td>${atencion.city}</td>
            <td>${precio+".000"}</td>
        `;
        tbody.appendChild(tr);
    });
}
function peticionCervezas() {
    return $.ajax({
        type: "GET",
        url: 'https://api.openbrewerydb.org/breweries',
        dataType: 'json',
    });
}

async function pintarTableTradicionalCerveza(){
    let data = await peticionTradicionalCerveza();
    const tbody = document.getElementById('bodyTradicionalCerveza');
    tbody.innerHTML = '';
    data.forEach(atencion => {
        let precio = precioRandom();
        const tr = document.createElement('tr');
        tr.ondblclick = () => {
            aggTableCompra(atencion.name,atencion.tagline,precio);
        }
        tr.innerHTML = `
            <td>${atencion.name}</td>
            <td>${atencion.tagline}</td>
            <td>${precio+".000"}</td>
        `;
        tbody.appendChild(tr);
    });
}
function peticionTradicionalCerveza() {
    return $.ajax({
        type: "GET",
        url: 'https://api.punkapi.com/v2/beers',
        dataType: 'json',
    });
}

async function pintarTableCocteles(){
    let data = await peticionCocteles();
    const tbody = document.getElementById('bodyCocteles');
    tbody.innerHTML = '';
    data.drinks.forEach(atencion => {
        let precio = precioRandom();
        const tr = document.createElement('tr');
        tr.ondblclick = () => {
            aggTableCompra(atencion.strDrink,atencion.strCategory,precio);
        }
        tr.innerHTML = `
            <td>${atencion.strDrink}</td>
            <td>${atencion.strCategory}</td>
            <td>${precio+".000"}</td>
        `;
        tbody.appendChild(tr);
    });
}
function peticionCocteles() {
    return $.ajax({
        type: "GET",
        url: 'https://www.thecocktaildb.com/api/json/v1/1/search.php?s=margarita',
        dataType: 'json',
    });
}

var contadorItemCompra = 0;

function aggTableCompra(nombre,ciudad,precio) {
    $('#bodyCompra').append(
    `<tr id="filaCompra${contadorItemCompra}" data-index="${contadorItemCompra}">
            <td class="text-center" width="2%">
                <button type="button" class="btn btn-danger" onclick="eliminarItemCompra(${precio},${contadorItemCompra})">Eliminar</button>
            </td> 
            <td>
                ${nombre}
            </td>
            <td>
                ${ciudad}
            </td>
            <td>
                ${precio+".000"}
            </td>
            <td>
                <button type="button" class="btn btn-danger" onclick="pintarTotalCompra(${precio},${contadorItemCompra},-1)"> - </button>
                <label id="cantidadCompra${contadorItemCompra}" name="cantidadCompra${contadorItemCompra}"></label>
                <button type="button" class="btn btn-success" onclick="pintarTotalCompra(${precio},${contadorItemCompra},1)"> + </button>
            </td>
    </tr>`
    );
  $('#cantidadCompra'+contadorItemCompra).val('1');
  document.getElementById('cantidadCompra'+contadorItemCompra).innerHTML= "1";
  contadorItemCompra++;

  pintarTotal(precio);
}

var total=0;

function eliminarItemCompra(precio,id) {

    let cantidad = parseInt( document.getElementById('cantidadCompra'+id).value );
    
    let sumar = precio*(cantidad);
    total = total-sumar;

    document.getElementById('totalCompra').innerHTML= total+".000";

    $('#filaCompra' + id).remove();
}

function pintarTotal(precio) {
    total+=precio;
    document.getElementById('totalCompra').innerHTML= total+".000";
}
function pintarTotalCompra(precio,id,ope) {

    let cantidad = parseInt( document.getElementById('cantidadCompra'+id).value );
    
    let sumar = parseInt(precio)*(cantidad+parseInt(ope));
    total = (total-(cantidad*parseInt(precio)))+sumar;

    document.getElementById('totalCompra').innerHTML= total+".000";
    $('#cantidadCompra'+id).val((cantidad+parseInt(ope)));
    document.getElementById('cantidadCompra'+id).innerHTML= (cantidad+parseInt(ope));
}

function precioRandom(){
    return Math.floor(Math.random() * (30 - 5) + 5);
}

function generarPDF() {
    
}

// inicializamos AOS libreria - css virtual
AOS.init();

let menu = document.getElementById('menu');
let menu_bar = document.getElementById('menu-bar');

menu_bar.addEventListener('click', function() {
  // classList lo q hace es remover una clase
  menu.classList.toggle('menu-toggle');
})